# Inventory Demo med Blazor

Projektet består av följande delar:

- Client, Blazor-klienten
- Server, Rest-API
- Shared, objekt som delas mellan client och server
- DataAccess, entiteter i databasen och access till databasen
- UseCases, logiken i backend



