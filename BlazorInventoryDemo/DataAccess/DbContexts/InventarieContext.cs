﻿using BlazorInventoryDemo.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlazorInventoryDemo.DataAccess.DbContexts
{
    public class InventarieContext : DbContext
    {
        public InventarieContext(DbContextOptions<InventarieContext> options) : base(options)
        {
        }

        public DbSet<Inventarie> Inventaries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Inventarie>().ToTable(nameof(Inventarie));
        }
    }
}
