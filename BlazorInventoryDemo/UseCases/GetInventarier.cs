﻿using BlazorInventoryDemo.DataAccess.DbContexts;
using BlazorInventoryDemo.Shared;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlazorInventoryDemo.UseCases
{
    public class GetInventarier
    {
        public class Request : IRequest<Response>
        {
        }

        public class Response
        {
            public IList<InventarieDto> Inventarier { get; set; }
        }



        public class Handler : IRequestHandler<Request, Response>
        {
            private readonly InventarieContext _inventarieContext;
            public Handler(InventarieContext inventarieContext)
            {
                _inventarieContext = inventarieContext;
            }


            public async Task<Response> Handle(Request request, CancellationToken cancellationToken)
            {
                _inventarieContext.Database.EnsureCreated();

                var inventarier = await _inventarieContext.Inventaries.ToListAsync();

                var response = new Response
                {
                    Inventarier = inventarier.Select(i => new InventarieDto { Namn = i.Namn, Anskaffningsdatum = i.Anskaffningsdatum, Kommentar = i.Kommentar }).ToList()
                };

                return response;

            }
        }
    }
}
